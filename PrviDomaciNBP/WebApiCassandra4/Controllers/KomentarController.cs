﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CassandraDataLayer.QueryEntities;
using CassandraDataLayer;

namespace WebApiCassandra4.Controllers
{
    public class KomentarController : ApiController
    {
        // GET api/komentar
        public IList<Komentar> Get()
        {
            DataProvider dp = new DataProvider();
            IList<Komentar> koms = dp.GetKomentari();
            return koms;
        }

        // GET api/komentar/0
        public Komentar Get(long id)
        {
            DataProvider dp = new DataProvider();
            Komentar kom = dp.GetKomentar(id);
            return kom;
        }

        // POST api/komentar
        public void Post(Komentar kom)
        {
            DataProvider dp = new DataProvider();
            dp.AddKomentar(kom);
        }

        // DELETE api/komentar/id
        public void Delete(long id)
        {
            DataProvider dp = new DataProvider();
            dp.DeleteKomentar(id);
        }
    }
}
