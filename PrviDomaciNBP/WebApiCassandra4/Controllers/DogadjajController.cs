﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CassandraDataLayer.QueryEntities;
using CassandraDataLayer;

namespace WebApiCassandra4.Controllers
{
    public class DogadjajController : ApiController
    {
        // GET api/dogadjaj
        public IList<Dogadjaj> Get()
        {
            DataProvider dp = new DataProvider();
            IList<Dogadjaj> dogs = dp.GetDogadjaji();
            return dogs;
        }
        
        // GET api/dogadjaj/0
        public Dogadjaj Get(long dogID)
        {
            DataProvider dp = new DataProvider();
            Dogadjaj dog = dp.GetDogadjaj(dogID);
            return dog;
        }


        // POST api/dogadjaj
        public void Post([FromBody] Dogadjaj dog, [FromBody] int pom, [FromBody] string username)
        {
            DataProvider dp = new DataProvider();
            dp.AddDogadjaj(dog);
            //dp.DodajVezuKorisnikDogadjaj(usernameKor, idtogadjaja, vrstadogadjaja);
            dp.DodajVezuKorisnikDogadjaj(username, dog.IdDogadjaja, pom);



        }

        // DELETE api/dogadjaj/0
        public void Delete(long id)
        {
            DataProvider dp = new DataProvider();
            dp.DeleteDogadjaj(id);
        }
        
    }
}
