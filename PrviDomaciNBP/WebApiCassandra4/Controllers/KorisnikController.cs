﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CassandraDataLayer.QueryEntities;
using CassandraDataLayer;

namespace WebApiCassandra4.Controllers
{
    public class KorisnikController : ApiController
    {

        // GET api/korisnik/Drasko
        public Korisnik Get([FromUri] string username)
        {
            DataProvider dp = new DataProvider();
            Korisnik kor = dp.GetKorisnik(username);
            return kor;
        }

        // GET api/korisnik
        public IList<Korisnik> Get()
        {
            DataProvider dp = new DataProvider();
            IList<Korisnik> kors = dp.GetKorisnici();
            return kors;
        }

        // POST api/korisnik
        public bool Post(Korisnik kor)
        {
            DataProvider dp = new DataProvider();
            if (dp.AddKorisnik(kor))
            {
                return true;
            }
            return false;
        }

        // DELETE api/korisnik/drasko
        public void Delete(string username)
        {
            DataProvider dp = new DataProvider();
            dp.DeleteKorisnik(username);
        }
    }
}
