﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CassandraDataLayer;
using CassandraDataLayer.QueryEntities;

namespace WebApiCassandra4.Controllers
{
    public class VezaDogadjajKomentarController : ApiController
    {
        // GET api/vezadogadjajkomentar/0
        public List<Komentar> Get([FromUri] int dogID)
        {
            DataProvider dp = new DataProvider();
            List<Komentar> koms = dp.GetKomentariIzVeze(dogID);
            return koms;
        }
    }
}
