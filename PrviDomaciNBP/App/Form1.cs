﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CassandraDataLayer;
using CassandraDataLayer.QueryEntities;

namespace App
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnGetDogadjaj_Click(object sender, EventArgs e)
        {
            DataProvider dp = new DataProvider();
            Dogadjaj dog = dp.GetDogadjaj(0);
            MessageBox.Show(dog.Naziv.ToString() + "; " + dog.Kategorija.ToString());
        }

        private void btnGetDogadjaji_Click(object sender, EventArgs e)
        {
            DataProvider dp = new DataProvider();
            List<Dogadjaj> dogs = dp.GetDogadjaji();

            foreach (var dog in dogs)
            {
                MessageBox.Show(dog.Naziv.ToString() + "; " + dog.Kategorija.ToString());
            }
        }

        private void btnAddDogadjaj_Click(object sender, EventArgs e)
        {
            DataProvider dp = new DataProvider();
            Dogadjaj dog = new Dogadjaj();
            dog.Naziv = "Naziv dogadjaja 1.";
            dog.Opis = "Opis dogadjaja 1.";
            dog.Kategorija = "Sport";
            dp.AddDogadjaj(dog);
        }

        private void btnUpdateDogadjaj_Click(object sender, EventArgs e)
        {
            Dogadjaj dog = new Dogadjaj();
            dog.IdDogadjaja = 0;        //FK 1/2
            dog.Kategorija = "Sport";   //FK 2/2
            dog.Naziv = "Izmenjeni naziv.";
            dog.Opis = "Izmenjeni opis.";
            dog.VremeOdrzavanja = "1.1.2016.";
            DataProvider dp = new DataProvider();
            dp.UpdateDogadjaj(dog);
            dp.UtvrdjivanjeAktuelnostiDogadjaja(dog);
        }

        private void btnDeleteDogadjaj_Click(object sender, EventArgs e)
        {
            DataProvider dp = new DataProvider();
            dp.DeleteDogadjaj(1);
        }

        #region Korisnik-button
        private void btnGetKorisnik_Click(object sender, EventArgs e)
        {
            DataProvider dp = new DataProvider();
            Korisnik kor = dp.GetKorisnik("Milorad");
            MessageBox.Show(kor.Username.ToString() + ", " + kor.Pass.ToString());
        }

        private void btnGetKorisnici_Click(object sender, EventArgs e)
        {
            DataProvider dp = new DataProvider();
            List<Korisnik> kor = dp.GetKorisnici();

            foreach (var k in kor)
            {
                MessageBox.Show(k.Username.ToString() + ", " + k.Pass.ToString());
            }
        }

        private void btnAddKorisnik_Click(object sender, EventArgs e)
        {
            DataProvider dp = new DataProvider();
            Korisnik k = new Korisnik();
            k.Username = "radojica";
            k.Pass = "zenamevara";
            k.Email = "radojicalimar11@gmail.com";
            k.Ime = "Radojica";
            k.Prezime = "Mucibaba";
            
            dp.AddKorisnik(k);
        }

        private void btnUpdateKorisnik_Click(object sender, EventArgs e)
        {
            // NE RADI. POTREBNO JE POSVETITI PAZNJU MALO OVOM METODU. MOZDA JE POTREBNO DA SE IPAK UpdateKorisnik-u prosledi pk, a ne objekat Korisnik?
            //Korisnik k = new Korisnik();
            //k.Username = "Milorad";
            //long br = 0;
            //k.KreiraniDogadjaji.Add(br);
            //br++;
            //k.KreiraniDogadjaji.Add(br);
            //DataProvider.UpdateKorisnik(k);

        }

        private void btnDeleteKorisnik_Click(object sender, EventArgs e)
        {
            DataProvider dp = new DataProvider();
            dp.DeleteKorisnik("Milorad");
        }
        #endregion

        #region Komentar-button
        private void btnGetKomentar_Click(object sender, EventArgs e)
        {
            DataProvider dp = new DataProvider();
            Komentar kom = dp.GetKomentar(0);
            MessageBox.Show(kom.Autor.ToString() + ", " + kom.Sadrzaj.ToString());
        }

        private void btnGetKomentare_Click(object sender, EventArgs e)
        {
            DataProvider dp = new DataProvider();
            List<Komentar> kom = dp.GetKomentari();

            foreach (var k in kom)
            {
                MessageBox.Show(k.Autor.ToString() + ", " + k.Sadrzaj.ToString());
            }
        }

        private void btnAddKomentar_Click(object sender, EventArgs e)
        {
            Komentar kom = new Komentar();
            kom.IdKomentara = 1;
            kom.Autor = "Drasko";
            kom.Sadrzaj = "Lopovi, vratite pare!";
            DataProvider dp = new DataProvider();
            dp.AddKomentar(kom);
        }

        private void btnDeleteKomentar_Click(object sender, EventArgs e)
        {
            DataProvider dp = new DataProvider();
            dp.DeleteKomentar(0);
        }



        #endregion

        //private void groupBox1_Enter(object sender, EventArgs e)
        //{

        //}
    }
}

/*
Spisak metoda:

    Klasa Dogadjaj:
        - public static Dogadjaj GetDogadjaj(long dogID)
        - public static List<Dogadjaj> GetDogadjaji()
        - public static bool AddDogadjaj(Dogadjaj d)
        - public static void UpdateDogadjaj(Dogadjaj dog)
        - public static void DeleteDogadjaj(long mID)

    Klasa Korisnik:
        - public static Korisnik GetKorisnik(string username)
        - public static List<Korisnik> GetKorisnici()
        - public static bool AddKorisnik(Korisnik k)
        - public static void UpdateKorisnik(Korisnik kor)
        - public static void DeleteKorisnik(string username)

    Klasa Komentar:
        - public static Komentar GetKomentar(long mID)
        - public static List<Komentar> GetKomentari()
        - public static bool AddKomentar(Komentar k)
        - public static void DeleteKomentar(long id)
*/