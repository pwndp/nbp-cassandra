﻿namespace App
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnDeleteDogadjaj = new System.Windows.Forms.Button();
            this.btnUpdateDogadjaj = new System.Windows.Forms.Button();
            this.btnAddDogadjaj = new System.Windows.Forms.Button();
            this.btnGetDogadjaji = new System.Windows.Forms.Button();
            this.btnGetDogadjaj = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnDeleteKorisnik = new System.Windows.Forms.Button();
            this.btnUpdateKorisnik = new System.Windows.Forms.Button();
            this.btnAddKorisnik = new System.Windows.Forms.Button();
            this.btnGetKorisnici = new System.Windows.Forms.Button();
            this.btnGetKorisnik = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnDeleteKomentar = new System.Windows.Forms.Button();
            this.btnAddKomentar = new System.Windows.Forms.Button();
            this.btnGetKomentare = new System.Windows.Forms.Button();
            this.btnGetKomentar = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnDeleteDogadjaj);
            this.groupBox1.Controls.Add(this.btnUpdateDogadjaj);
            this.groupBox1.Controls.Add(this.btnAddDogadjaj);
            this.groupBox1.Controls.Add(this.btnGetDogadjaji);
            this.groupBox1.Controls.Add(this.btnGetDogadjaj);
            this.groupBox1.Location = new System.Drawing.Point(35, 21);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(247, 161);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Dogadjaj";
            // 
            // btnDeleteDogadjaj
            // 
            this.btnDeleteDogadjaj.Location = new System.Drawing.Point(68, 114);
            this.btnDeleteDogadjaj.Name = "btnDeleteDogadjaj";
            this.btnDeleteDogadjaj.Size = new System.Drawing.Size(96, 23);
            this.btnDeleteDogadjaj.TabIndex = 4;
            this.btnDeleteDogadjaj.Text = "DeleteDogadjaj";
            this.btnDeleteDogadjaj.UseVisualStyleBackColor = true;
            this.btnDeleteDogadjaj.Click += new System.EventHandler(this.btnDeleteDogadjaj_Click);
            // 
            // btnUpdateDogadjaj
            // 
            this.btnUpdateDogadjaj.Location = new System.Drawing.Point(123, 72);
            this.btnUpdateDogadjaj.Name = "btnUpdateDogadjaj";
            this.btnUpdateDogadjaj.Size = new System.Drawing.Size(96, 23);
            this.btnUpdateDogadjaj.TabIndex = 3;
            this.btnUpdateDogadjaj.Text = "UpdateDogadjaj";
            this.btnUpdateDogadjaj.UseVisualStyleBackColor = true;
            this.btnUpdateDogadjaj.Click += new System.EventHandler(this.btnUpdateDogadjaj_Click);
            // 
            // btnAddDogadjaj
            // 
            this.btnAddDogadjaj.Location = new System.Drawing.Point(23, 72);
            this.btnAddDogadjaj.Name = "btnAddDogadjaj";
            this.btnAddDogadjaj.Size = new System.Drawing.Size(84, 23);
            this.btnAddDogadjaj.TabIndex = 2;
            this.btnAddDogadjaj.Text = "AddDogadjaj";
            this.btnAddDogadjaj.UseVisualStyleBackColor = true;
            this.btnAddDogadjaj.Click += new System.EventHandler(this.btnAddDogadjaj_Click);
            // 
            // btnGetDogadjaji
            // 
            this.btnGetDogadjaji.Location = new System.Drawing.Point(123, 30);
            this.btnGetDogadjaji.Name = "btnGetDogadjaji";
            this.btnGetDogadjaji.Size = new System.Drawing.Size(96, 23);
            this.btnGetDogadjaji.TabIndex = 1;
            this.btnGetDogadjaji.Text = "GetDogadjaji";
            this.btnGetDogadjaji.UseVisualStyleBackColor = true;
            this.btnGetDogadjaji.Click += new System.EventHandler(this.btnGetDogadjaji_Click);
            // 
            // btnGetDogadjaj
            // 
            this.btnGetDogadjaj.Location = new System.Drawing.Point(23, 30);
            this.btnGetDogadjaj.Name = "btnGetDogadjaj";
            this.btnGetDogadjaj.Size = new System.Drawing.Size(84, 23);
            this.btnGetDogadjaj.TabIndex = 0;
            this.btnGetDogadjaj.Text = "GetDogadjaj";
            this.btnGetDogadjaj.UseVisualStyleBackColor = true;
            this.btnGetDogadjaj.Click += new System.EventHandler(this.btnGetDogadjaj_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnDeleteKorisnik);
            this.groupBox2.Controls.Add(this.btnUpdateKorisnik);
            this.groupBox2.Controls.Add(this.btnAddKorisnik);
            this.groupBox2.Controls.Add(this.btnGetKorisnici);
            this.groupBox2.Controls.Add(this.btnGetKorisnik);
            this.groupBox2.Location = new System.Drawing.Point(325, 21);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(247, 161);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Korisnik";
            // 
            // btnDeleteKorisnik
            // 
            this.btnDeleteKorisnik.Location = new System.Drawing.Point(68, 114);
            this.btnDeleteKorisnik.Name = "btnDeleteKorisnik";
            this.btnDeleteKorisnik.Size = new System.Drawing.Size(96, 23);
            this.btnDeleteKorisnik.TabIndex = 4;
            this.btnDeleteKorisnik.Text = "DeleteKorisnik";
            this.btnDeleteKorisnik.UseVisualStyleBackColor = true;
            this.btnDeleteKorisnik.Click += new System.EventHandler(this.btnDeleteKorisnik_Click);
            // 
            // btnUpdateKorisnik
            // 
            this.btnUpdateKorisnik.Location = new System.Drawing.Point(123, 72);
            this.btnUpdateKorisnik.Name = "btnUpdateKorisnik";
            this.btnUpdateKorisnik.Size = new System.Drawing.Size(96, 23);
            this.btnUpdateKorisnik.TabIndex = 3;
            this.btnUpdateKorisnik.Text = "UpdateKorisnik";
            this.btnUpdateKorisnik.UseVisualStyleBackColor = true;
            this.btnUpdateKorisnik.Click += new System.EventHandler(this.btnUpdateKorisnik_Click);
            // 
            // btnAddKorisnik
            // 
            this.btnAddKorisnik.Location = new System.Drawing.Point(23, 72);
            this.btnAddKorisnik.Name = "btnAddKorisnik";
            this.btnAddKorisnik.Size = new System.Drawing.Size(84, 23);
            this.btnAddKorisnik.TabIndex = 2;
            this.btnAddKorisnik.Text = "AddKorisnik";
            this.btnAddKorisnik.UseVisualStyleBackColor = true;
            this.btnAddKorisnik.Click += new System.EventHandler(this.btnAddKorisnik_Click);
            // 
            // btnGetKorisnici
            // 
            this.btnGetKorisnici.Location = new System.Drawing.Point(123, 30);
            this.btnGetKorisnici.Name = "btnGetKorisnici";
            this.btnGetKorisnici.Size = new System.Drawing.Size(96, 23);
            this.btnGetKorisnici.TabIndex = 1;
            this.btnGetKorisnici.Text = "GetKorisnici";
            this.btnGetKorisnici.UseVisualStyleBackColor = true;
            this.btnGetKorisnici.Click += new System.EventHandler(this.btnGetKorisnici_Click);
            // 
            // btnGetKorisnik
            // 
            this.btnGetKorisnik.Location = new System.Drawing.Point(23, 30);
            this.btnGetKorisnik.Name = "btnGetKorisnik";
            this.btnGetKorisnik.Size = new System.Drawing.Size(84, 23);
            this.btnGetKorisnik.TabIndex = 0;
            this.btnGetKorisnik.Text = "GetKorisnik";
            this.btnGetKorisnik.UseVisualStyleBackColor = true;
            this.btnGetKorisnik.Click += new System.EventHandler(this.btnGetKorisnik_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnDeleteKomentar);
            this.groupBox3.Controls.Add(this.btnAddKomentar);
            this.groupBox3.Controls.Add(this.btnGetKomentare);
            this.groupBox3.Controls.Add(this.btnGetKomentar);
            this.groupBox3.Location = new System.Drawing.Point(35, 217);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(247, 121);
            this.groupBox3.TabIndex = 5;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Komentar";
            // 
            // btnDeleteKomentar
            // 
            this.btnDeleteKomentar.Location = new System.Drawing.Point(123, 72);
            this.btnDeleteKomentar.Name = "btnDeleteKomentar";
            this.btnDeleteKomentar.Size = new System.Drawing.Size(96, 23);
            this.btnDeleteKomentar.TabIndex = 4;
            this.btnDeleteKomentar.Text = "DeleteKomentar";
            this.btnDeleteKomentar.UseVisualStyleBackColor = true;
            this.btnDeleteKomentar.Click += new System.EventHandler(this.btnDeleteKomentar_Click);
            // 
            // btnAddKomentar
            // 
            this.btnAddKomentar.Location = new System.Drawing.Point(23, 72);
            this.btnAddKomentar.Name = "btnAddKomentar";
            this.btnAddKomentar.Size = new System.Drawing.Size(84, 23);
            this.btnAddKomentar.TabIndex = 2;
            this.btnAddKomentar.Text = "AddKomentar";
            this.btnAddKomentar.UseVisualStyleBackColor = true;
            this.btnAddKomentar.Click += new System.EventHandler(this.btnAddKomentar_Click);
            // 
            // btnGetKomentare
            // 
            this.btnGetKomentare.Location = new System.Drawing.Point(123, 30);
            this.btnGetKomentare.Name = "btnGetKomentare";
            this.btnGetKomentare.Size = new System.Drawing.Size(96, 23);
            this.btnGetKomentare.TabIndex = 1;
            this.btnGetKomentare.Text = "GetKomentare";
            this.btnGetKomentare.UseVisualStyleBackColor = true;
            this.btnGetKomentare.Click += new System.EventHandler(this.btnGetKomentare_Click);
            // 
            // btnGetKomentar
            // 
            this.btnGetKomentar.Location = new System.Drawing.Point(23, 30);
            this.btnGetKomentar.Name = "btnGetKomentar";
            this.btnGetKomentar.Size = new System.Drawing.Size(84, 23);
            this.btnGetKomentar.TabIndex = 0;
            this.btnGetKomentar.Text = "GetKomentar";
            this.btnGetKomentar.UseVisualStyleBackColor = true;
            this.btnGetKomentar.Click += new System.EventHandler(this.btnGetKomentar_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(618, 383);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnDeleteDogadjaj;
        private System.Windows.Forms.Button btnUpdateDogadjaj;
        private System.Windows.Forms.Button btnAddDogadjaj;
        private System.Windows.Forms.Button btnGetDogadjaji;
        private System.Windows.Forms.Button btnGetDogadjaj;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnDeleteKorisnik;
        private System.Windows.Forms.Button btnUpdateKorisnik;
        private System.Windows.Forms.Button btnAddKorisnik;
        private System.Windows.Forms.Button btnGetKorisnici;
        private System.Windows.Forms.Button btnGetKorisnik;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnDeleteKomentar;
        private System.Windows.Forms.Button btnAddKomentar;
        private System.Windows.Forms.Button btnGetKomentare;
        private System.Windows.Forms.Button btnGetKomentar;
    }
}

