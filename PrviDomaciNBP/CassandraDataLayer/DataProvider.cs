﻿using Cassandra;
using CassandraDataLayer.QueryEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CassandraDataLayer
{
    //public static class DataProvider
    //{
    //    #region Hotel
    //    public static Hotel GetHotel(string hotelID)
    //    {
    //        ISession session = SessionManager.GetSession();
    //        Hotel hotel = new Hotel();

    //        if (session == null)
    //            return null;

    //        Row hotelData = session.Execute("select * from \"Hotel\" where \"hotelID\"='1'").FirstOrDefault();

    //        if(hotelData != null)
    //        {
    //            hotel.hotelID = hotelData["hotelID"] != null ? hotelData["hotelID"].ToString() : string.Empty;
    //            hotel.name = hotelData["name"] != null ? hotelData["name"].ToString() : string.Empty;
    //            hotel.address = hotelData["address"] != null ? hotelData["address"].ToString() : string.Empty;
    //            hotel.city = hotelData["city"] != null ? hotelData["city"].ToString() : string.Empty;
    //            hotel.phone = hotelData["phone"] != null ? hotelData["phone"].ToString() : string.Empty;
    //            hotel.state = hotelData["state"] != null ? hotelData["state"].ToString() : string.Empty;
    //            hotel.zip = hotelData["zip"] != null ? hotelData["zip"].ToString() : string.Empty;
    //        }

    //        return hotel;
    //    }

    //    public static List<Hotel> GetHotels()
    //    {
    //        ISession session = SessionManager.GetSession();
    //        List<Hotel> hotels = new List<Hotel>();


    //        if (session == null)
    //            return null;

    //        var hotelsData = session.Execute("select * from \"Hotel\"");


    //        foreach(var hotelData in hotelsData)
    //        {
    //            Hotel hotel = new Hotel();
    //            hotel.hotelID = hotelData["hotelID"] != null ? hotelData["hotelID"].ToString() : string.Empty;
    //            hotel.name = hotelData["name"] != null ? hotelData["name"].ToString() : string.Empty;
    //            hotel.address = hotelData["address"] != null ? hotelData["address"].ToString() : string.Empty;
    //            hotel.city = hotelData["city"] != null ? hotelData["city"].ToString() : string.Empty;
    //            hotel.phone = hotelData["phone"] != null ? hotelData["phone"].ToString() : string.Empty;
    //            hotel.state = hotelData["state"] != null ? hotelData["state"].ToString() : string.Empty;
    //            hotel.zip = hotelData["zip"] != null ? hotelData["zip"].ToString() : string.Empty;
    //            hotels.Add(hotel);
    //        }



    //        return hotels;
    //    }

    //    public static void AddHotel(string hotelID)
    //    {
    //        ISession session = SessionManager.GetSession();

    //        if (session == null)
    //            return;

    //        RowSet hotelData = session.Execute("insert into \"Hotel\" (\"hotelID\", address, city, name, phone, state, zip)  values ('" + hotelID +"', 'Vozda Karadjordja 12', 'Nis', 'Grand', '123', 'Srbija', '18000')");

    //    }

    //    public static void DeleteHotel(string hotelID)
    //    {
    //        ISession session = SessionManager.GetSession();
    //        Hotel hotel = new Hotel();

    //        if (session == null)
    //            return;

    //        RowSet hotelData = session.Execute("delete from \"Hotel\" where \"hotelID\" = '" + hotelID + "'");

    //    }

    //    #endregion

    //    #region Room

    //    public static Room GetRoom(string hotelID, string roomID)
    //    {
    //        ISession session = SessionManager.GetSession();
    //        Room room = new Room();

    //        if (session == null)
    //            return null;

    //        Row roomData = session.Execute("select * from \"Room\" where \"hotelID\"='" + hotelID +"' and \"roomID\"='" + roomID + "'").FirstOrDefault();

    //        if (roomData != null)
    //        {
    //            room.hotelID = roomData["hotelID"] != null ? roomData["hotelID"].ToString() : string.Empty;
    //            room.roomID = roomData["roomID"] != null ? roomData["roomID"].ToString() : string.Empty;
    //            room.hottub = roomData["hottub"] != null ? roomData["hottub"].ToString() : string.Empty;
    //            room.num = roomData["num"] != null ? roomData["num"].ToString() : string.Empty;
    //            room.rate = roomData["rate"] != null ? roomData["rate"].ToString() : string.Empty;
    //            room.tv = roomData["tv"] != null ? roomData["tv"].ToString() : string.Empty;
    //            room.type = roomData["type"] != null ? roomData["type"].ToString() : string.Empty;
    //        }

    //        return room;
    //    }

    //    public static List<Room> GetRooms()
    //    {
    //        ISession session = SessionManager.GetSession();
    //        List<Room> rooms = new List<Room>();

    //        if (session == null)
    //            return null;

    //        var roomsData = session.Execute("select * from \"Room\"");

    //        foreach(var row in roomsData)
    //        {
    //            Room room = new Room();
    //            room.hotelID = row["hotelID"] != null ? row["hotelID"].ToString() : string.Empty;
    //            room.roomID = row["roomID"] != null ? row["roomID"].ToString() : string.Empty;
    //            room.hottub = row["hottub"] != null ? row["hottub"].ToString() : string.Empty;
    //            room.num = row["num"] != null ? row["num"].ToString() : string.Empty;
    //            room.rate = row["rate"] != null ? row["rate"].ToString() : string.Empty;
    //            room.tv = row["tv"] != null ? row["tv"].ToString() : string.Empty;
    //            room.type = row["type"] != null ? row["type"].ToString() : string.Empty;

    //            rooms.Add(room);
    //        }

    //        return rooms;
    //    }

    //    public static void AddRoom(string hotelID, string roomID)
    //    {
    //        ISession session = SessionManager.GetSession();

    //        if (session == null)
    //            return;

    //        RowSet roomData = session.Execute("insert into \"Room\"(\"hotelID\",\"roomID\", hottub, num, rate, tv, \"type\") values ('" + hotelID + "', '" + roomID + "', 'yes', '101', '25', 'yes', 'appartment')");

    //    }

    //    public static void DeleteRoom(string hotelID, string roomID)
    //    {
    //        ISession session = SessionManager.GetSession();

    //        if (session == null)
    //            return;

    //        RowSet roomData = session.Execute("delete from \"Room\" where \"hotelID\" = '" + hotelID + "' and \"roomID\" = '" + roomID + "'");

    //    }

    //    #endregion

    //    #region Geust

    //    public static Guest GetGuest(string phone)
    //    {
    //        ISession session = SessionManager.GetSession();
    //        Guest guest = new Guest();

    //        if (session == null)
    //            return null;

    //        Row guestData = session.Execute("select * from \"Guest\" where phone='" + phone + "'").FirstOrDefault();

    //        if (guestData != null)
    //        {
    //            guest.phone = guestData["phone"] != null ? guestData["phone"].ToString() : string.Empty;
    //            guest.email = guestData["email"] != null ? guestData["email"].ToString() : string.Empty;
    //            guest.fname = guestData["fname"] != null ? guestData["fname"].ToString() : string.Empty;
    //            guest.lname = guestData["lname"] != null ? guestData["lname"].ToString() : string.Empty;
    //        }

    //        return guest;
    //    }

    //    public static List<Guest> GetGuests()
    //    {
    //        ISession session = SessionManager.GetSession();
    //        List<Guest> guests = new List<Guest>();

    //        if (session == null)
    //            return null;

    //        var guestsData = session.Execute("select * from \"Guest\"");


    //        foreach (var guestData in guestsData)
    //        {
    //            Guest guest = new Guest();
    //            guest.phone = guestData["phone"] != null ? guestData["phone"].ToString() : string.Empty;
    //            guest.email = guestData["email"] != null ? guestData["email"].ToString() : string.Empty;
    //            guest.fname = guestData["fname"] != null ? guestData["fname"].ToString() : string.Empty;
    //            guest.lname = guestData["lname"] != null ? guestData["lname"].ToString() : string.Empty;

    //            guests.Add(guest);
    //        }


    //        return guests;
    //    }

    //    public static void AddGuest(string phone)
    //    {
    //        ISession session = SessionManager.GetSession();

    //        if (session == null)
    //            return;

    //        RowSet guestData = session.Execute("insert into \"Guest\"(phone, email, fname, lname) values ('" + phone + "', 'email@email.com', 'test', 'test')");

    //    }

    //    public static void DeleteGuest(string phone)
    //    {
    //        ISession session = SessionManager.GetSession();

    //        if (session == null)
    //            return;

    //        RowSet guestData = session.Execute("delete from \"Guest\" where phone = '" + phone + "'");

    //    }

    //    #endregion

    //    #region Reservation

    //    public static Reservation GetReservation(string resID)
    //    {
    //        ISession session = SessionManager.GetSession();
    //        Reservation reservation = new Reservation();

    //        if (session == null)
    //            return null;

    //        Row reservationData = session.Execute("select * from \"Reservation\" where \"resID\"='" + resID +"'").FirstOrDefault();

    //        if (reservationData != null)
    //        {
    //            reservation.hotelID = reservationData["hotelID"] != null ? reservationData["hotelID"].ToString() : string.Empty;
    //            reservation.roomID = reservationData["roomID"] != null ? reservationData["roomID"].ToString() : string.Empty;
    //            reservation.resID = reservationData["resID"] != null ? reservationData["resID"].ToString() : string.Empty;
    //            reservation.arrive = reservationData["arrive"] != null ? reservationData["arrive"].ToString() : string.Empty;
    //            reservation.depart = reservationData["depart"] != null ? reservationData["depart"].ToString() : string.Empty;
    //            reservation.name = reservationData["name"] != null ? reservationData["name"].ToString() : string.Empty;
    //            reservation.phone = reservationData["phone"] != null ? reservationData["phone"].ToString() : string.Empty;
    //            reservation.rate = reservationData["rate"] != null ? reservationData["rate"].ToString() : string.Empty;
    //        }

    //        return reservation;
    //    }

    //    public static List<Reservation> GetReservations()
    //    {
    //        ISession session = SessionManager.GetSession();
    //        List<Reservation> reservations = new List<Reservation>();

    //        if (session == null)
    //            return null;

    //        var reservationsData = session.Execute("select * from \"Reservation\"");


    //        foreach (Row reservationData in reservationsData)
    //        {
    //            Reservation reservation = new Reservation();
    //            reservation.hotelID = reservationData["hotelID"] != null ? reservationData["hotelID"].ToString() : string.Empty;
    //            reservation.roomID = reservationData["roomID"] != null ? reservationData["roomID"].ToString() : string.Empty;
    //            reservation.resID = reservationData["resID"] != null ? reservationData["resID"].ToString() : string.Empty;
    //            reservation.arrive = reservationData["arrive"] != null ? reservationData["arrive"].ToString() : string.Empty;
    //            reservation.depart = reservationData["depart"] != null ? reservationData["depart"].ToString() : string.Empty;
    //            reservation.name = reservationData["name"] != null ? reservationData["name"].ToString() : string.Empty;
    //            reservation.phone = reservationData["phone"] != null ? reservationData["phone"].ToString() : string.Empty;
    //            reservation.rate = reservationData["rate"] != null ? reservationData["rate"].ToString() : string.Empty;

    //            reservations.Add(reservation);
    //        }


    //        return reservations;
    //    }

    //    public static void AddReservation(string resID)
    //    {
    //        ISession session = SessionManager.GetSession();

    //        if (session == null)
    //            return;

    //        RowSet reservationData = session.Execute("insert into \"Reservation\"(\"resID\", \"hotelID\",\"roomID\", arrive, depart, name, phone, rate) values ('" + resID + "', '1', '101', 'date', 'date', 'test', '018181818', '25')");

    //    }

    //    public static void DeleteReservation(string resID)
    //    {
    //        ISession session = SessionManager.GetSession();

    //        if (session == null)
    //            return;

    //        RowSet reservationData = session.Execute("delete from \"Reservation\" where \"resID\" = '" + resID + "'");

    //    }

    //    #endregion

    //}
    public class DataProvider
    {
        #region Dogadjaj
        public Dogadjaj GetDogadjaj(long dogID)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return null;

            Dogadjaj dogadjaj = new Dogadjaj();

            Row pribavljeniDogadjaj = session.Execute("select * from \"Dogadjaj\" where \"iddogadjaja\"=" + dogID).FirstOrDefault();

            if (pribavljeniDogadjaj != null)
            {
                dogadjaj.IdDogadjaja = dogID;
                dogadjaj.Naziv = pribavljeniDogadjaj["naziv"] != null ? pribavljeniDogadjaj["naziv"].ToString() : string.Empty;
                dogadjaj.Opis = pribavljeniDogadjaj["opis"] != null ? pribavljeniDogadjaj["opis"].ToString() : string.Empty;
                dogadjaj.VremeOdrzavanja = pribavljeniDogadjaj["vremeodrzavanja"] != null ? pribavljeniDogadjaj["vremeodrzavanja"].ToString() : string.Empty;
                dogadjaj.BrojPrijavljenih = pribavljeniDogadjaj["brojprijavljenih"] != null ? Int32.Parse(pribavljeniDogadjaj["brojprijavljenih"].ToString()) : 0;  //KLATI SE!!!
                dogadjaj.Attachment = pribavljeniDogadjaj["attachment"] != null ? pribavljeniDogadjaj["attachment"].ToString() : string.Empty;
                dogadjaj.Kategorija = pribavljeniDogadjaj["kategorija"] != null ? pribavljeniDogadjaj["kategorija"].ToString() : string.Empty;

                long[] pomocniKomentari = pribavljeniDogadjaj["komentari"] != null ? (long[])pribavljeniDogadjaj["komentari"] : null;    //Pokusaj za pribavljanje liste
                if (pomocniKomentari != null)       //Ovo mora da se uvede jer ako je lista null, javlja se error u foreach-u ispod
                {
                    dogadjaj.Komentari = new List<long>();
                    foreach (var n in pomocniKomentari)     //Ovde se javlja greska ako je pomocniKomentari==null
                        dogadjaj.Komentari.Add(n);
                }
            }

            return dogadjaj;
        }

        public List<Dogadjaj> GetDogadjaji()
        {
            ISession session = SessionManager.GetSession();
            
            if (session == null)
                return null;
            List<Dogadjaj> ls = new List<Dogadjaj>();

            var pribavljeniDogadjaji = session.Execute("select * from \"Dogadjaj\"");

            foreach (var dog in pribavljeniDogadjaji)
            {
                Dogadjaj dogadjaj = new Dogadjaj();

                dogadjaj.IdDogadjaja = dog["iddogadjaja"] != null ? Int64.Parse(dog["iddogadjaja"].ToString()) : 0;
                dogadjaj.Naziv = dog["naziv"] != null ? dog["naziv"].ToString() : string.Empty;
                dogadjaj.Opis = dog["opis"] != null ? dog["opis"].ToString() : string.Empty;
                dogadjaj.VremeOdrzavanja = dog["vremeodrzavanja"] != null ? dog["vremeodrzavanja"].ToString() : string.Empty;
                dogadjaj.BrojPrijavljenih = dog["brojprijavljenih"] != null ? Int32.Parse(dog["brojprijavljenih"].ToString()) : 0;  //KLATI SE!!!
                dogadjaj.Attachment = dog["attachment"] != null ? dog["attachment"].ToString() : string.Empty;
                dogadjaj.Kategorija = dog["kategorija"] != null ? dog["kategorija"].ToString() : string.Empty;

                long[] pomocniKomentari = dog["komentari"] != null ? (long[])dog["komentari"] : null;
                if (pomocniKomentari != null)       //Isti razlog kao u metodu GetDogadaj
                {
                    dogadjaj.Komentari = new List<long>();
                    foreach (var n in pomocniKomentari)
                        dogadjaj.Komentari.Add(n);
                }

                ls.Add(dogadjaj);
            }

            return ls;
        }

        public bool AddDogadjaj(Dogadjaj d)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return false;
            Row temp = session.Execute("select max(iddogadjaja) from \"Dogadjaj\"").FirstOrDefault();
            int brID;
            if (temp[0] == null)       //JER KAD SE PRVI PUT UCITA, TEMP[0] JE NULL
                brID = 0;
            else
            {
                brID = Int32.Parse(temp[0].ToString());
                brID++; 
            }
            //RowSet dod = session.Execute("insert into \"Dogadjaj\" (iddogadjaja, naziv, vremeodrzavanja, opis, brojprijavljenih, attachment) values " +
            //                              "("+brID+ ", '" + d.Naziv + "', '" + d.VremeOdrzavanja +"', '" + d.Opis +"', 0, " + d.Attachment + "')");
            RowSet dod = session.Execute("insert into \"Dogadjaj\" (iddogadjaja, naziv, vremeodrzavanja, arhiviran, opis, brojprijavljenih, attachment, kategorija) values " +
                                          "(" + brID + ", '" + d.Naziv + "', '" + d.VremeOdrzavanja + "', " + false + ", '" + d.Opis + "', 0, '" + d.Attachment + "', '" + d.Kategorija + "')");
            return true;
        }

        public void UpdateDogadjaj(Dogadjaj dog)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return;

            string tStr = "naziv='"+ dog.Naziv+ "', vremeodrzavanja='"+ dog.VremeOdrzavanja+"', opis='"+ dog.Opis+"', brojprijavljenih="+ dog.BrojPrijavljenih+", attachment='"+ dog.Attachment+"', arhiviran=" + dog.Arhiviran;

            Row tmp = session.Execute("update \"Dogadjaj\" set "+tStr+" where iddogadjaja="+ dog.IdDogadjaja + " and kategorija='"+dog.Kategorija + "'").FirstOrDefault();
        }

        public void DeleteDogadjaj(long mID)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return;

            RowSet tmp = session.Execute("delete from \"Dogadjaj\" where iddogadjaja="+mID);
        }

        public bool DaLiJeZavrsenDogadjaj(Dogadjaj d)
        {
            string vreme = d.VremeOdrzavanja;
            int mesec;
            int dan;
            int godina;
            int p = 0;

            p = vreme.IndexOf(".");
            mesec = Int32.Parse(vreme.Substring(0, p));
            vreme = vreme.Substring(p+1);

            p = vreme.IndexOf(".");
            dan = Int32.Parse(vreme.Substring(0, p));
            vreme = vreme.Substring(p+1);

            godina = Int32.Parse(vreme.Substring(0, 4));

            String today = DateTime.Now.ToString("dd-MM-yyyy");
            int danT, mesecT, godinaT;

            p = today.IndexOf("-");
            danT = Int32.Parse(today.Substring(0, p));
            today = today.Substring(p+1);

            p = today.IndexOf("-");
            mesecT = Int32.Parse(today.Substring(0,p));
            today = today.Substring(p+1);

            godinaT = Int32.Parse(today.Substring(0, 4));

            if (godinaT > godina)
                return true;
            if (mesecT > mesec)
                return true;
            if (danT > dan)
                return true;

            return false;
        }
        public void UtvrdjivanjeAktuelnostiDogadjaja(Dogadjaj d)
        {
            if (DaLiJeZavrsenDogadjaj(d))   //Dogadjaj vise nije aktivan, treba promeniti njegov atribut
            {
                //Ovde mi treba fja, arhiviraj!
                Arhiviraj(d);
                return;
            }
            //Dogadjaj je aktuelan

        }
        public void Arhiviraj(Dogadjaj dog)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return;

            session.Execute("update \"Dogadjaj\" set arhiviran=" + true + " where iddogadjaja=" + dog.IdDogadjaja + " and kategorija='" + dog.Kategorija + "'").FirstOrDefault();
        }

        #endregion

        #region Korisnik

        public Korisnik GetKorisnik(string username)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return null;

            Korisnik kor = new Korisnik();

            Row temp = session.Execute("select * from \"Korisnik\" where \"username\"='" + username + "'").FirstOrDefault();

            if (temp != null)
            {
                kor.Username = username;
                kor.Pass = temp["pass"] != null ? temp["pass"].ToString() : string.Empty;
                kor.Email = temp["email"] != null ? temp["email"].ToString() : string.Empty;
                kor.Ime = temp["ime"] != null ? temp["ime"].ToString() : string.Empty;
                kor.Prezime = temp["prezime"] != null ? temp["prezime"].ToString() : string.Empty;

            }

            return kor;
        }

        public List<Korisnik> GetKorisnici()
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return null;
            List<Korisnik> ls = new List<Korisnik>();
            RowSet korisnici = session.Execute("select * from \"Korisnik\"");

            foreach (var temp in korisnici)
            {
                Korisnik kor = new Korisnik();
                kor.Username = temp["username"] !=null ? temp["username"].ToString() : string.Empty;
                kor.Pass = temp["pass"] != null ? temp["pass"].ToString() : string.Empty;
                kor.Email = temp["email"] != null ? temp["email"].ToString() : string.Empty;
                kor.Ime = temp["ime"] != null ? temp["ime"].ToString() : string.Empty;
                kor.Prezime = temp["prezime"] != null ? temp["prezime"].ToString() : string.Empty;

                ls.Add(kor);
            }
            return ls;
        }

        public bool AddKorisnik(Korisnik k)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return false;

            Korisnik tmp = GetKorisnik(k.Username);

            if (tmp.Username == null)
            {
                RowSet dod = session.Execute("insert into \"Korisnik\" (username,pass,email,ime,prezime) values " +
                                          "('" + k.Username + "','" + k.Pass +"','" + k.Email + "','" + k.Ime + "','" + k.Prezime + "')");
                return true;
            }

            return false;
        }

        public void UpdateKorisnik(Korisnik kor)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return;
        }

        public void DeleteKorisnik(string username)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return;

            RowSet tmp = session.Execute("delete from \"Korisnik\" where username='" +username+"'");
        }

        #endregion

        #region Komentar

        public Komentar GetKomentar(long mID)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return null;

            Komentar kom = new Komentar();

            Row temp = session.Execute("select * from \"Komentar\" where idkomentara="+mID).FirstOrDefault();

            if (temp != null)
            {
                kom.IdKomentara = mID;
                kom.Sadrzaj = temp["sadrzaj"] != null ? temp["sadrzaj"].ToString() : string.Empty;
                kom.Autor = temp["autor"] != null ? temp["autor"].ToString() : string.Empty;
            }

            return kom;
        }

        public List<Komentar> GetKomentari()    //Sve
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return null;
            List<Komentar> komentari = new List<Komentar>();

            RowSet temp = session.Execute("select * from \"Komentar\"");

            foreach(var kom in temp)
            {
                Komentar k = new Komentar();
                k.IdKomentara = kom["idkomentara"] != null ? Int64.Parse(kom["idkomentara"].ToString()) : 0;
                k.Sadrzaj = kom["sadrzaj"] != null ? kom["sadrzaj"].ToString() : string.Empty;
                k.Autor = kom["autor"] != null ? kom["autor"].ToString() : string.Empty;
                komentari.Add(k);
            }

            return komentari;
        }

        public bool AddKomentar(Komentar k)
        {

            ISession session = SessionManager.GetSession();

            if (session == null)
                return false;

            RowSet dod = session.Execute("insert into \"Komentar\" (idkomentara, sadrzaj, autor) values " +
                                          "(" + k.IdKomentara + ",'" + k.Sadrzaj + "','" + k.Autor + "')");

            return true;
        }

        public void DeleteKomentar(long id)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return;

            RowSet tmp = session.Execute("delete from \"Komentar\" where idkomentara=" + id + "");
        }

        #endregion

        #region Mesovite fje
        public List<Komentar> GetKomentariIzVeze(int idDog)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return null;

            List<Komentar> komentari = new List<Komentar>();
            List<int> listaIdKomentara = new List<int>();

            //RowSet temp = session.Execute("select * from \"VezaDogadjajKomentar\" where iddogadjaj=" + idDog + "");
            RowSet temp = session.Execute("select * from \"VezaDogadjajKomentar\"");
            if (temp == null)
            {
                return null;
            }
            foreach (var v in temp)
            {
                if (Int32.Parse(v["iddogadjaj"].ToString()) == idDog)
                {
                    int pom = Int32.Parse(v["idkomentar"].ToString());
                    listaIdKomentara.Add(pom);
                }
            }

            // Sada, na osnovu listeIdKomentara koje sam pribavio iz zajednicke tabela, treba da pribavim
            // komentare, dakle citave objekte, iz tabele Komentar.

            for (int i = 0; i < listaIdKomentara.Count; i++)
            {
                Row kom = session.Execute("select * from \"Komentar\" where idkomentara=" + listaIdKomentara[i]).FirstOrDefault();
                Komentar k = new Komentar();
                k.IdKomentara = kom["idkomentara"] != null ? Int64.Parse(kom["idkomentara"].ToString()) : 0;
                k.Sadrzaj = kom["sadrzaj"] != null ? kom["sadrzaj"].ToString() : string.Empty;
                k.Autor = kom["autor"] != null ? kom["autor"].ToString() : string.Empty;
                komentari.Add(k);
            }
            return komentari;

        }

        public void DodajVezuKorisnikDogadjaj(string username, long dogID, int vrstaDog)
        {
            // vrstaDog:
            // 0 - kreirane
            // 1 - omiljene

            ISession session = SessionManager.GetSession();

            if (session == null)
                return;


            // Pribavljamo trenutni maximum, inkrementiramo ga, i onda ga koristimo za insert into
            Row temp = session.Execute("select max(id) from \"VezaDogadjajKorisnik\"").FirstOrDefault();
            int id=0;
            if (temp[0] == null)       //JER KAD SE PRVI PUT UCITA, TEMP[0] JE NULL
                id = 0;
            else
            {
                id = Int32.Parse(temp[0].ToString());
                id++;
            }

            RowSet dod = session.Execute("insert into \"VezaDogadjajKorisnik\" (id, iddogveza, korusername, vrstadogadjaja) values " +
                                          "(" + id + "," + dogID + ",'" + username + "'," + vrstaDog + ")");
        }
        #endregion


    }


}

/*
- Ukoliko se prilikom izvrsavanja kverija ocekuje SAMO jedan rezultat , taj rezultat se treba smestiti u promenljivu tipa
Row, dok ukoliko se vraca vise vrednosti ili ne znamo sta ce da nam se vrati, koristimo RowSet (eng. Skup redova).
- Prilikom koriscenja insert into, update ili delete kverija, ukoliko radimo sa nekom kolonom koja je string,
 njenu vrednost moramo obuhvatiti izmedju apostrofa(pr: 'vrednostkolone'). Ukoliko se radi sa integer vrednostima
 neke kolone, vrednost je direktno dodeljena (bez ikakve potrebe za apostrofima).
 - Koriscenjem agregatnih fcija (min,max,sum,avg,count), privaljanje vrednosti iz promenljive u kojoj se smestila
 vrednost se radi na sledeci nacin: promenljiva[0] . Pritom, trebamo da kastujemo tu vrednost u odgovarajuci oblik
 pre daljeg koriscenja.
 - Liste se u kasandri pamte u formatu: [ vrednost1, vrednost2, vrednost3 ,...] . Prilikom pribaljanja istih, lista
 se pamti kao niz elemenata u c# unutar promenljive koju smo smestili listu iz baze.


Spisak metoda:

    Klasa Dogadjaj:
        - public static Dogadjaj GetDogadjaj(long dogID)
        - public static List<Dogadjaj> GetDogadjaji()
        - public static bool AddDogadjaj(Dogadjaj d)
        - public static void UpdateDogadjaj(Dogadjaj dog)
        - public static void DeleteDogadjaj(long mID)

    Klasa Korisnik:
        - public static Korisnik GetKorisnik(string username)
        - public static List<Korisnik> GetKorisnici()
        - public static bool AddKorisnik(Korisnik k)
        - public static void UpdateKorisnik(Korisnik kor)
        - public static void DeleteKorisnik(string username)

    Klasa Komentar:
        - public static Komentar GetKomentar(long mID)
        - public static List<Komentar> GetKomentari()
        - public static bool AddKomentar(Komentar k)
        - public static void DeleteKomentar(long id)


*/
