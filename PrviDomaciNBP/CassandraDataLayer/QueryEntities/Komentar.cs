﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CassandraDataLayer.QueryEntities
{
    public class Komentar
    {
        public long IdKomentara { get; set; }
        public string Sadrzaj { get; set; }
        public string Autor { get; set; }   //Korisnik
    }
}
