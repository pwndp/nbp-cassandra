﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CassandraDataLayer.QueryEntities
{
    public class Dogadjaj
    {
        public long IdDogadjaja { get; set; }
        public string Naziv { get; set; } //Naziv dogadjaja
        public string VremeOdrzavanja { get; set; }
        public string Opis { get; set; }
        public int BrojPrijavljenih { get; set; }
        public List<long> Komentari { get; set; }
        public string Attachment { get; set; }
        public string Kategorija { get; set; }
        public bool Arhiviran { get; set; }

    }
}
