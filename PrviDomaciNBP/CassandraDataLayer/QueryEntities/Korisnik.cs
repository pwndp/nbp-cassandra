﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CassandraDataLayer.QueryEntities
{
    public class Korisnik
    {
        public string Username { get; set; }
        public string Ime { get; set; }
        public string Prezime { get; set; }
        public string Email { get; set; }
        public string Pass { get; set; }
        //public List<long> KreiraniDogadjaji { get; set; }
        //public List<long> OmiljeniDogadjaji { get; set; }

    }
}
