var host = "http://localhost:53253/api/";
var $http = angular.injector(["ng"]).get("$http");

/****************************/
/********** KLASE ***********/
/****************************/

/* KratakDogadjaj koristimo za prikaz dogadjaja sa kratkim opisom na stranici za listanje  */
class KratakDogadjaj{

constructor(ime,mID,kor,opis){
		this.imeDogadjaja=ime;
		this.ID=mID;
		this.KreiraoKorisnik=kor;
		this.kratakOpis=opis;
	}

}

/* CeliDogadjaj koristimo za prikaz dogadjaja sa celim opisom i prilogom na stranici za prikaz dogadjaja */
class CeliDogadjaj{

  constructor(ime,mID,kor,opis,attach,noOfSubscribed,categ,date){
    if (arguments.length==0){
    this.ImeDogadjaja = "Test dogadjaj";
    this.ID=123;
    this.KreiraoKorisnik="David";
    this.Opis = "Ovo je korisnicki dogadjaj. On je kreiran samo da zauzima mesto na ekranu. Ovo ne bi trebalo da ima nikakvog smisla."
     +"Ne osudjuj hejteru. Ovo je deo projekta za napredne baze podataka na sedmom semestru smera racunarstvo i informatika elektronskog"
     + "fakulteta u Nisu. Naravno, kao i pretpostavljeno, u pitanju je Niski univerzitet.";
    this.Prilog = "https://pbs.twimg.com/profile_images/469217937862037504/yd3-lohT_400x400.jpeg"; //http://www.cats.org.uk/uploads/branches/211/5507692-cat-m.jpg";
    this.BrojPrijavljenih=0;
    this.Kategorija = "Provod";
    this.Datum="11-11-2011";
    }
    else{
    this.ImeDogadjaja = arguments[0];
    this.ID=arguments[1];
    this.KreiraoKorisnik=arguments[2];
    this.Opis = arguments[3];
    this.Prilog = arguments[4];
    this.BrojPrijavljenih = arguments[5];
    this.Kategorija = arguments[6];
    this.Datum=arguments[7];
    }    
  };

}

/*  */
class Komentar{
  constructor(user,koment,id){
    this.Korisnik = user;
    this.Komentar = koment;
    this.ID = id;
  }
}

/****************************/
/********* OSTALO ***********/
/****************************/

// Funckija za preuzimanje scope
function getScope(ctrlName) {
    var sel = 'div[ng-controller="' + ctrlName + '"]';
    return angular.element(sel).scope();
}

// Funkcija za postavljanje kolacica
function setCookie(cname, cvalue, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
  var expires = "expires="+d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

// Pribavljanje kolacica sa zadatim imenom
function getCookie(cname) {
  var name = cname + "=";
  var ca = document.cookie.split(';');
  for(var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

// Funkcija za izmenu kategorije na ekranu za dodavanje dogadjaja
izmeniKategoriju = function(name){
	var ob = document.getElementById("vrstaKategorije");
	ob.textContent=name;
}

// Funkcija za ispitivanje prijavljivanja korisnika na login ekranu
pokusajPrijavljivanje = function(){
  var userInp = document.getElementById("loginUsername");
  var passInp = document.getElementById("loginPass");
  var user = userInp.value;
  var pass = passInp.value;
  proveriBazuZaKorisnika(user,pass);
}

// Funkcija za registrovanje korisnika na registracionom ekranu
pokusajRegistracije = function(){
  // Vrednosti
  var nameStr = document.getElementById("imeKorisnika").value;
  var lastNameStr = document.getElementById("prezimeKorisnika").value;
  var emailStr = document.getElementById("emailKorisnika").value;
  var userStr = document.getElementById("usernameKorisnika").value;
  var passStr = document.getElementById("lozinkaKorisnika").value;
  var outText = document.getElementById("afterText");

  // Provera da li je neko od polja prazno
  if (nameStr=="" || nameStr==undefined || nameStr.toLowerCase()=="null"){
    outText.innerHTML="Polje za ime je prazno.";
    return;
  }
  else if (lastNameStr=="" || lastNameStr==undefined || lastNameStr.toLowerCase()=="null"){
    outText.innerHTML = "Polje za prezime je prazno";
    return;
  }
  else if (emailStr=="" || emailStr==undefined || emailStr.toLowerCase()=="null"){
    outText.innerHTML = "Email adresa je prazna.";
    return;
  }
  else if (userStr=="" || userStr==undefined || userStr.toLowerCase()=="null"){
    outText.innerHTML = "Korisničko ime ne može da bude prazno.";
    return;
  }
  else if (passStr=="" || passStr==undefined || passStr.toLowerCase()=="null"){
    outText.innerHTML = "Lozinka ne može da bude prazna.";
    return;
  }

  // Provera korisnika
  var stanje = dodajKorisnikaUBazu(userStr,passStr,emailStr,nameStr,lastNameStr);
  stanje.then(
    function(response){
      var uspeh = response;
      
      if (uspeh){
      //Redirectuj korisnika na login screen
        var sec = 3;
        var d = new Date();
        d.setTime(d.getTime() + (sec * 1000));
        var expires = "expires="+d.toUTCString();
        document.cookie = "loggedIn=3;" + expires + ";path=/";
        window.location.href = "#!prijava";
      }
      else {
        outText.innerHTML = "Korisničko ime je zauzeto";
        return;
      }

    },
    function(response){
      outText.innerHTML = "Nastala je greška prilikom registracije.";
      return;
    }
  );
}


// Ispituje da li je zadati korinsik validan (i njegov password)
proveriBazuZaKorisnika = function(user,pass){
  $http.get(host+"korisnik?username="+user).then(
    function(data){
      var podaci = data.data;
     // alert(user+" "+pass+" ; "+podaci.Username+" "+podaci.Pass);
      if (podaci.Username===user && podaci.Pass===pass){
          setCookie("username",user,1);
          setCookie("userIme",podaci.Ime,1);
          setCookie("userPrezime",podaci.Prezime,1);
          setCookie("userEmail",podaci.Email,1);
          setCookie("loggedIn",2,1); // 2 je validno stanje
          window.location.href = "#!home"; // Vrati se na homepage
          location.reload(); // Osvezi i primeni podesavanja
        }
      else{
        loseLogovanje();
      }
    },
    function(){
      loseLogovanje();
    }
  );
}

// Prikazuje poruku za sluzaj da je lose logovanje
loseLogovanje = function(){
  var sec = 2;
  var d = new Date();
  d.setTime(d.getTime() + (sec * 1000));
  var expires = "expires="+d.toUTCString();
  document.cookie = "loggedIn=1;" + expires + ";path=/";
  window.location.href = "#!prijava";
}

// Dodaje novog korisnika u bazu
dodajKorisnikaUBazu = function(user,pass,email,name,lastname){
// Vraca 0 ako nastane neka greska
// Vraca 1 ako postoji username
// Vraca 2 ako ne postoji username
var myData = "{ 'Username': '"+user+"', 'Pass':'"+pass+"', 'Email' :'"+email+"', 'Ime':'"+name+"',"+
" 'Prezime': '"+lastname+"' }";
$http.get({ url: host+"korisnik", method: "POST", data: myData, headers:{
  'Content-Type' : 'application/x-www-form-urlencoded'} });
}

// Vraca dogadjaj sa zadatim id-jem
vratiDogadjajSaID = function(mID){
  // Ova funkcija bi trebalo da vrati i naziv korisnika
  // Dok treba imati na umu da ona zapravo vraca njegov id
  // Pozvati jos jedan kveri zbog toga ili embedovati taj podatak u kveri na serveru
  return $http.get(host+"dogadjaj?dogID="+mID);
}

// Vratca komentare trenutnog dogadjaja
vratiKomentare = function(idDog){
  return $http.get(host+"vezadogadjajkomentar?dogID="+idDog);
}

// Vraca kreirane dogadjaje korisnika
vratiKreiraneDog = function(mID){
  var arr=[];
  arr = testFunkcijaZaKratkeDogadjaje();
  return arr;
}

// Vraca omiljene dogadjaje korisnika
vratiOmiljeneDog = function(mID){
  var arr=[];
  arr = testFunkcijaZaKratkeDogadjaje();
  return arr;
}

// Ukloni ovo
testFunkcijaZaKratkeDogadjaje = function(){
  var dogadjaji=[];
  for (var i = 1; i < 10; i++) {
    var username = "korisnik"+ i;
    var eventName = "dogadjaj"+i;
    var kratakOpis = "Ovo je kratak opis dogadjaja sa id-jem "+ i + ". On sluzi samo kao placeholder, i"+
             " protivzakonito je koristiti ga u bilo koje druge svrhe. Hvala na razumevanju."
    var kd = new KratakDogadjaj(eventName,i,username,kratakOpis);
    dogadjaji.push(kd);
  }
  return dogadjaji;
}

// Odjavljivanje korisnika
odjaviKorisnika = function(){
  setCookie("username","",0);
  setCookie("loggedIn",0,0);
  setCookie("userEmail","",0);
  setCookie("userIme","",0);
  setCookie("userPrezime","",0);
  window.location.href = "#!home"; // Vrati se na homepage
  location.reload(); // Osvezi i primeni podesavanja 
}

// Dodaje trenutni dogadjaj kod korisnika ako ne postoji
dodajOmiljeni = function(){

}

// Uklanja trenutni dogadjaj kod korisnika ako postoji
ukloniOmiljeni = function(){

}

// Vrsi proveru da li dogadjaj pripada korisniku
proveriDaLiJeMojDogadjaj = function(user,dogID){
  
  return false;
}

proveriDaLiJeMedjuOmiljenima = function(user,dogID){
  // 1 jeste medju omiljenima
  // 0 nije medju omiljenima
  

  return 1;
}

