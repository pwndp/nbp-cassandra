
// Definicija promenljivih
var app = angular.module('kasandraApp', ['ngRoute']);
var controllers = {};

// Sada definisemo kontrolere
controllers.homeController = function ($scope){
    $scope;
}

controllers.navigacioniController = function($scope){
	$scope.textToDisplay1= "Prijavi se";
	$scope.textInBetween= "|";
	$scope.textToDisplay2= "Registruj se";
	$scope.ulogovanKorisnik=false;

	var provera = function(){

		var korisnikJeLoginovan= function(){
		var username = getCookie("username");
		var stanje = getCookie("loggedIn");
		if (username != "" && stanje==2)
			return true;
		setCookie("username","",0);
		setCookie("loggedIn",0,0);
		return false;
		}

		if (korisnikJeLoginovan())
		{
			$scope.ulogovanKorisnik=true;
			$scope.textToDisplay1= getCookie("username");
			$scope.textInBetween= "";
			$scope.textToDisplay2= "";
		}
	}

	provera();
}

controllers.dogadjajiController = function($scope){
	// Implementacija kontrolera ovde
	$scope.pogledajDogadjaj = function(val){
	  setCookie("curDog",val,1);
	  window.location.href = "#!dogadjaj";	  
	};

	// Dummy data
	var dogadjaji=[];
	for (var i = 1; i < 10; i++) {
		var username = "korisnik"+ i;
		var eventName = "dogadjaj"+i;
		var kratakOpis = "Ovo je kratak opis dogadjaja sa id-jem "+ i + ". On sluzi samo kao placeholder, i"+
						 " protivzakonito je koristiti ga u bilo koje druge svrhe. Hvala na razumevanju."
		var kd = new KratakDogadjaj(eventName,i,username,kratakOpis);
		dogadjaji.push(kd);
	}
	$scope.Dogadjaji = dogadjaji;
}

controllers.noviDogadjajController = function($scope){
	// Implementacija kontrolera ovde
	var state=getCookie("loggedIn");
	if (state!=2){
	window.location.href = "#!home";		
	}

	$scope.submitujPodatke = function(){
		var nazivDogadjaja = document.getElementsByName("nazivDogadjaja")[0].value;
		var kategorija = document.getElementById("vrstaKategorije").textContent;
		alert(nazivDogadjaja+" "+kategorija);
	};

}

controllers.korisnikController = function($scope){
	// Implementacija kontrolera ovde
	
	// Funkcija kojom pregeldavamo dogadjaj
	$scope.pogledajDogadjaj = function(val){
	  setCookie("curDog",val,1);
	  window.location.href = "#!dogadjaj";	  
	}
	
	var idKor = getCookie("username");
	var kreirani = vratiKreiraneDog(idKor);
	var omiljeni = vratiOmiljeneDog(idKor);
	$scope.Ime=getCookie("userIme");
	$scope.Prezime=getCookie("userPrezime");
	$scope.Email=getCookie("userEmail");
	$scope.User=idKor;
	$scope.kDogadjaji = kreirani;
	$scope.oDogadjaji = omiljeni;
	$scope.kreiraniSize = kreirani.length;
	$scope.omiljeniSize = omiljeni.length;
}

controllers.prikazDogadjajaController = function($scope){
	// Implementacija kontrolera ovde
	var mojDogadjaj=false;
	$scope.omiljenoDugmeStanje=-1;
	function proveriUlogovanost(){
		var dug = document.getElementById("objaviKomentar");
		var txt = document.getElementById("tekstKomentara");

		if (getCookie("loggedIn")!=2){
		// Nije ulogovan
		dug.disabled=true;
		txt.disabled=true;
		mojDogadjaj=true; // Ne prikazuj dugme
		}
		else{ // Prikazi dugme
		dug.disabled=false;
		txt.disabled=false;
		ulogovan=true;
		mojDogadjaj = proveriDaLiJeMojDogadjaj(getCookie("username"),getCookie("curDog"));
		var dodat=-1;
		if (!mojDogadjaj){
			dodat = proveriDaLiJeMedjuOmiljenima(getCookie("username"),getCookie("curDog"));
			$scope.omiljenoDugmeStanje = dodat;
		}}
	}

	proveriUlogovanost();

	$scope.mojDogadjaj=mojDogadjaj;

	//Vrati se nazad ako ne postoji kolacic
  	var idDogadj = getCookie("curDog");
  	if (idDogadj<1 || idDogadj==undefined || idDogadj==""){
  		window.location.href = "#!dogadjaji";
		return;	
  	}

  	// Zatrazi dogadjaj
  	var dog = vratiDogadjajSaID(idDogadj);
  	$scope.didLoad=false;

  	dog.then(
    function(data){
	    var korPodaci = data.data;

	    // Zatrazi podatke
	    $scope.dogIme = korPodaci.Naziv;
	    $scope.korIme = "Korisnik";
	    $scope.opisDog = korPodaci.Opis;
	    // Attachment
	    var attach = korPodaci.Attachment;
	    if (attach=="" || attach==undefined || attach=="null" || attach.length<4){
		  $scope.prilogValidity=false;
		  $scope.Prilog="";
		}
		else{
		 $scope.prilogValidity=true;
		 $scope.Prilog = attach;
		}

	    $scope.BrPrijava= korPodaci.BrojPrijavljenih;
	    $scope.Kategorija = korPodaci.Kategorija;
	    $scope.VremeOdrzavanja = korPodaci.VremeOdrzavanja;
	    $scope.IDDogadjaja = idDogadj;
	    $scope.$apply();
		
		// Zatrazi komentare
		var kom = vratiKomentare(idDogadj);
		kom.then(
			function(data){
				$scope.Komentari = kom;
				$scope.$apply();
			},
			function(){
				$scope.Komentari=[];
				$scope.$apply();
			}
		);
		/* End of inner */
    },
    function(){
      window.location.href = "#!dogadjaji";
		return;	
    });
	
}

controllers.registrujSeController = function($scope){
	// Nista ne treba da bude implementirano ovde (barem kolko ja znam)
}

controllers.prijaviSeController = function($scope){
	// Implementacija kontrolera ovde
	var msg="";
	var msg2="";
	var err=getCookie("loggedIn");
	if (err==1){
		msg="Neispravno korisničko ime ili lozinka!";
	}
	else if (err==3){
		msg="Kreiranje naloga je uspešno."
		msg2="Sada možete da se prijavite.";
	}
	$scope.outMessage=msg;
	$scope.outMessage2=msg2;
}

// Onda dodajemo kontrolere
app.controller(controllers);

// Ovde definisemo rute (kontrolere i templejte) za razlicite putanje
app.config(['$routeProvider', function ($routeProvider){
  $routeProvider.when('/',{
      controller: 'homeController',
      templateUrl: 'Partials/home.html'
  })
  .when('/dogadjaji',{
      controller: 'dogadjajiController',
      templateUrl: 'Partials/dogadjaji.html'
  })
  .when('/kreirajDogadjaj',{
  	  controller:'noviDogadjajController',
  	  templateUrl: 'Partials/kreirajDogadjaj.html'
  })
  .when('/korisnik',{
  	  controller:'korisnikController',
  	  templateUrl: 'Partials/korisnik.html'
  })
  .when('/registracija',{
  	  controller:'registrujSeController',
  	  templateUrl: 'Partials/registrujse.html'
  })
  .when('/prijava',{
  	  controller:'prijaviSeController',
  	  templateUrl: 'Partials/prijavise.html'
  })
  .when('/dogadjaj',{
  	  controller:'prikazDogadjajaController',
  	  templateUrl: 'Partials/prikazDogadjaja.html'
  })
  .otherwise({redirectTo: '/'})
}]);

